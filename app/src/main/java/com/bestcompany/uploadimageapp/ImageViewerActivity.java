package com.bestcompany.uploadimageapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bestcompany.uploadimageapp.Views.TouchImageView;
import com.bumptech.glide.Glide;

public class ImageViewerActivity extends AppCompatActivity {
	TouchImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_viewer);
		imageView = findViewById(R.id.touch_imageview);
		String url = getIntent().getExtras().getString("link");
		Glide.with(this).load(url).into(imageView);
	}
}
