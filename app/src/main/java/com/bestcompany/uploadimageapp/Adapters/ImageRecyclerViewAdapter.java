package com.bestcompany.uploadimageapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bestcompany.uploadimageapp.ImageViewerActivity;
import com.bestcompany.uploadimageapp.R;
import com.bestcompany.uploadimageapp.Structure.Data;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;

import java.util.ArrayList;
import java.util.List;

public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ViewHolder> {
	List<Data> dataSet = new ArrayList<>();
	Context context;

	public ImageRecyclerViewAdapter (Context context, List<Data> dataList) {
		this.context = context;
		dataSet = dataList;
	}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
			Log.v("RecyclerBindView", "Setting up: " + i);
			final Data data = dataSet.get(i);
			Glide.with(context).load(data.url).into(viewHolder.imageView);
			if (viewHolder.textView != null)
				viewHolder.textView.setText(data.name);

			viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, ImageViewerActivity.class);
					intent.putExtra("link", data.url);
					context.startActivity(intent);
				}
			});
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            textView = imageView.findViewById(R.id.text_name);
        }
    }
}

