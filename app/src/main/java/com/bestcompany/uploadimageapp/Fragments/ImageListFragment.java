package com.bestcompany.uploadimageapp.Fragments;


import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bestcompany.uploadimageapp.Adapters.ImageRecyclerViewAdapter;
import com.bestcompany.uploadimageapp.R;
import com.bestcompany.uploadimageapp.Structure.Data;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageListFragment extends Fragment {

    RecyclerView recyclerView;
    ImageRecyclerViewAdapter imageRecyclerViewAdapter;

    List<Data> dataList = new ArrayList<>();

    public ImageListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_list, container, false);
        recyclerView = view.findViewById(R.id.recycler);

			FirebaseDatabase database = FirebaseDatabase.getInstance("https://upload-image-534b8.firebaseio.com/");
			DatabaseReference myRef = database.getReference("images");

        ValueEventListener postListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            	dataList.clear();

                for (DataSnapshot shot : dataSnapshot.getChildren()) {
                	Data data = shot.getValue(Data.class);
                	dataList.add(data);
									Log.v("Data", "Name: " + data.name + " url:" + data.url);
								}
								Collections.reverse(dataList);
                if (imageRecyclerViewAdapter == null)
									imageRecyclerViewAdapter = new ImageRecyclerViewAdapter(getActivity(), dataList);
                else
                	imageRecyclerViewAdapter.notifyDataSetChanged();

							LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
							layoutManager.setOrientation(LinearLayout.VERTICAL);
							recyclerView.setLayoutManager(layoutManager);
							recyclerView.setAdapter(imageRecyclerViewAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

			myRef.addValueEventListener(postListener);
			return view;
    }

}
