package com.bestcompany.uploadimageapp.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bestcompany.uploadimageapp.MainActivity;
import com.bestcompany.uploadimageapp.R;
import com.bestcompany.uploadimageapp.Structure.Data;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.bestcompany.uploadimageapp.MainActivity.croppedBitmap;
import static com.bestcompany.uploadimageapp.MainActivity.imageFile;
import static com.bestcompany.uploadimageapp.MainActivity.imageUri;

public class UploadImageFragment extends Fragment {


    public UploadImageFragment() {
        // Required empty public constructor
    }

    private final int REQUEST_CAMERA_PERMISSION = 10;
    private final int REQUEST_STORAGE_PERMISSION = 20;

    private ImageButton takePicture;
    private CropImageView imageView;
    private Button cropUpload;
    private ImageView postView;

    private View.OnClickListener onClickListenerBeforeUpload;
    private View.OnClickListener onClickListenerAfterUpload;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.v("onCreateView", "Called");

        final View view = inflater.inflate(R.layout.fragment_upload_image, container, false);
        imageView = view.findViewById(R.id.image_upload);
        postView = view.findViewById(R.id.image_postview);
        cropUpload = (Button) view.findViewById(R.id.crop);
        takePicture = (ImageButton) view.findViewById(R.id.upload_button);

        setView();

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    MultiplePermissionsListener snackbarMultiplePermissionsListener =
                            SnackbarOnAnyDeniedMultiplePermissionsListener.Builder
                                    .with(view, "Camera and storage access is needed to take pictures of your dog")
                                    .withOpenSettingsButton("Settings")
                                    .withCallback(new Snackbar.Callback() {
                                        @Override
                                        public void onShown(Snackbar snackbar) {
                                            // Event handler for when the given Snackbar has been dismissed
                                        }
                                        @Override
                                        public void onDismissed(Snackbar snackbar, int event) {
                                            // Event handler for when the given Snackbar is visible
                                        }
                                    })
                                    .build();
                    MultiplePermissionsListener permissionListener = new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted())
                                takePicture();
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                        }
                    };
                    MultiplePermissionsListener compositeListener = new CompositeMultiplePermissionsListener(permissionListener, snackbarMultiplePermissionsListener);

                    Dexter.withActivity(getActivity())
                            .withPermissions(
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ).withListener(compositeListener)
                            .withErrorListener(new PermissionRequestErrorListener() {
                        @Override
                        public void onError(DexterError error) {

                        }
                    }).check();
                } else {
                    takePicture();
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.d("CameraIntent", "Got Result");
            MainActivity.CurrentState = MainActivity.State.WaitingCrop;
            setView();
        }
    }

    private void takePicture () {
        setupFile();
        if (imageFile == null)
            return;

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        takePictureIntent.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        imageUri = FileProvider.getUriForFile(getActivity(), "com.bestcompany.uploadimageapp.fileprovider", imageFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri );
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void setupFile () {
        try {
            imageFile = createFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createFile () throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currFormatter = new SimpleDateFormat("dMy-hms");
        String date = currFormatter.format(calendar.getTime()).trim();
        final File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile("img-" + date, ".jpg", dir);
        return image;
    }

    @SuppressLint("ResourceAsColor")
    private void setView () {

        onClickListenerAfterUpload = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.CurrentState = MainActivity.State.WaitingUploadButton;
                setView();
            }
        };

        onClickListenerBeforeUpload = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setOnCropImageCompleteListener(new CropImageView.OnCropImageCompleteListener() {
                    @Override
                    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
                        if (result.getError() == null) {
                            croppedBitmap = result.getBitmap();
                            MainActivity.CurrentState = MainActivity.State.WaitingUpload;
                            setView();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] data = baos.toByteArray();

                            FirebaseStorage storage = FirebaseStorage.getInstance("gs://upload-image-534b8.appspot.com");
                            StorageReference storageRef = storage.getReference();
                            final StorageReference imagesRef = storageRef.child("images/" + imageFile.getName());
                            imagesRef.putBytes(data).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    return imagesRef.getDownloadUrl();
                                }
                            })
                              .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                  @Override
                                  public void onComplete(@NonNull Task<Uri> task) {
                                      if (task.isSuccessful()) {
                                          Uri downloadUri = task.getResult();
                                          Log.v("UploadTask", "Success");
                                          FirebaseDatabase database = FirebaseDatabase.getInstance("https://upload-image-534b8.firebaseio.com/");
                                          DatabaseReference myRef = database.getReference("images");

                                          Data uploadInfo = new Data();
                                          uploadInfo.name = imageFile.getName();
                                          uploadInfo.url = downloadUri.toString();
                                          String key = myRef.push().getKey();
                                          myRef.child(key).setValue(uploadInfo);

                                          MainActivity.CurrentState = MainActivity.State.WaitReset;
                                          setView();
                                      }
                                  }
                              })
                              .addOnFailureListener(new OnFailureListener() {
                                  @Override
                                  public void onFailure(@NonNull Exception e) {
                                      e.printStackTrace();
                                  }
                              });
                        }
                    }
                });
                imageView.getCroppedImageAsync();
            }
        };

        if (MainActivity.CurrentState == MainActivity.State.WaitingUploadButton) {
            takePicture.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            cropUpload.setVisibility(View.GONE);
            postView.setVisibility(View.GONE);

            cropUpload.setOnClickListener(onClickListenerBeforeUpload);
        } else if (MainActivity.CurrentState == MainActivity.State.WaitingCrop) {
            takePicture.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            cropUpload.setVisibility(View.VISIBLE);
            postView.setVisibility(View.GONE);

            cropUpload.setBackgroundColor(getResources().getColor(android.R.color.white));
            cropUpload.setOnClickListener(onClickListenerBeforeUpload);
            cropUpload.setText(R.string.crop_and_upload);
            imageView.setImageUriAsync(imageUri);
        } else if (MainActivity.CurrentState == MainActivity.State.WaitingUpload) {
            takePicture.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            cropUpload.setVisibility(View.VISIBLE);
            postView.setVisibility(View.VISIBLE);

            cropUpload.setText("Uploading Please Wait");
            cropUpload.setEnabled(false);
            postView.setImageBitmap(croppedBitmap);
        } else if (MainActivity.CurrentState == MainActivity.State.WaitReset) {
            takePicture.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            cropUpload.setVisibility(View.VISIBLE);
            postView.setVisibility(View.VISIBLE);

            postView.setImageBitmap(croppedBitmap);
            cropUpload.setEnabled(true);
            cropUpload.setText("Uploaded");
            cropUpload.setOnClickListener(onClickListenerAfterUpload);
        }
    }
}
