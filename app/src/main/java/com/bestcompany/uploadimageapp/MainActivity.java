package com.bestcompany.uploadimageapp;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.bestcompany.uploadimageapp.Fragments.ImageListFragment;
import com.bestcompany.uploadimageapp.Fragments.UploadImageFragment;
import com.google.firebase.FirebaseApp;

import java.io.File;

public class MainActivity extends AppCompatActivity {

	public enum State {
		WaitingUploadButton,
		WaitingCrop,
		WaitingUpload,
		WaitReset
	}

	BottomNavigationView navigation;

	public static State CurrentState = State.WaitingUploadButton;
	public static File imageFile;
	public static Uri imageUri;
	public static Bitmap croppedBitmap;

	private int currentNavigationItem = -1;

    public Toolbar MyToolbar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
									currentNavigationItem = R.id.navigation_home;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new UploadImageFragment()).commit();
                    return true;
									case R.id.navigation_dashboard:
                		currentNavigationItem = R.id.navigation_dashboard;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new ImageListFragment()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(this);

        MyToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        setSupportActionBar(MyToolbar);
        getSupportActionBar().setTitle(R.string.app_name);

        navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null || !savedInstanceState.containsKey("navigation")) {
					Log.d("SavedState", "Does not have key");
					currentNavigationItem = R.id.navigation_home;
					navigation.setSelectedItemId(currentNavigationItem);
				} else {
					Log.d("SavedState", "Found saved state");
					currentNavigationItem = savedInstanceState.getInt("navigation");
					navigation.setSelectedItemId(currentNavigationItem);
				}
    }

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("navigation", currentNavigationItem);
	}
}
